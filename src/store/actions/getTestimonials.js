import axios from "axios"

export const getTestimonials = () => async dispatch => {
    dispatch({
        type: "GET_TESTIMONIALS"
    })
    try {
        const res = await axios.get("https://wknd-take-home-challenge-api.herokuapp.com/testimonial")
        dispatch({
            type:"GET_TESTIMONIALS",
            payload: res.data
        })
    } catch(error) {
        console.log(error)
    }
}