import { combineReducers } from "redux"
import getTestimonials from "./getTestimonials"
import getTips from "./getTips"

const rootReducers = combineReducers({
    getTestimonials,getTips
})
export default rootReducers;