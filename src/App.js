import React from 'react';
import { Provider } from "react-redux"
import "./App.css"
import store from "./store"
import Header from "./layouts/Header"
import Weekend from "./components/Weekend"
import Definition from "./components/Definition"
import Pov from "./components/Pov"
import Help from "./components/Help"
import Footer from "./layouts/Footer"

function App() {
  return (
    <Provider store={store}>
        <React.Fragment>
          <Header/>
          <Weekend />
          <Definition />
          <Pov />
          <Help />
          <Footer />
        </React.Fragment>
    </Provider>
  );
}

export default App;
