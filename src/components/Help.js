import React, { useEffect } from "react"
import { useSelector, useDispatch } from "react-redux";
import "../assets/styles/help.scss"
import {getTips} from "../store/actions/getTips"

const Help = () => {
    const dispatch = useDispatch()
    const tips = useSelector(state => state.getTips.tips)

    useEffect(() => {
        dispatch(getTips())
    }, [dispatch] )

    return(
        <React.Fragment>
            <div className="help">
                <h1>Help & Tips</h1>
                <div className="help-cards">
                {tips && tips.map(tips =>
                <div className="help-cards-container">
                    <div className="help-cards-image">                
                        <img src={tips.image} alt="tips" />
                    </div>
                    <div className="help-cards-box">
                    </div>
                    <div className="help-cards-text">
                        <h4>{tips.title}</h4>
                        <input type="image" src={require("../assets/images/oval-icon.png")} alt="oval" />
                    </div>
                </div>
                )}
                </div>
                <div className="set">
                    <img src={require("../assets/images/group-3.png")} alt="group=3" />
                    <div className="set-text">
                        <h1>You’re all set.</h1>
                        <p>The wise man therefore always holds in these matters to this 
                            principle of <br/>selection.
                        </p>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Help;