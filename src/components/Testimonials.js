import React, {useEffect} from "react"
import { useSelector, useDispatch } from "react-redux";
import "../assets/styles/testimonials.scss"
import {getTestimonials} from "../store/actions/getTestimonials"
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss'
import 'swiper/swiper-bundle.css'
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y} from 'swiper';

const Testimonials = () => {
    const dispatch = useDispatch()
    const testimonials = useSelector(state => state.getTestimonials.testimonials)
    SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

    useEffect(()=> {
        dispatch(getTestimonials())
    }, [dispatch] )

    return(
        <React.Fragment>
            <div className="testimonials">
                <div className="testimonials-card">
                <Swiper
                    spaceBetween={20}
                    slidesPerView={1}
                    navigation
                    pagination={{ clickable: true }}
                    scrollbar={{ draggable: true }}
                    onSwiper={(swiper) => console.log(swiper)}
                    onSlideChange={() => console.log('slide change')}
                >
                {testimonials && testimonials.map(testimonials =>
                    <SwiperSlide> 
                        <div className="testimonials-card-container">
                            <h1>{testimonials.by}</h1>
                            <p>{testimonials.testimony}</p>
                        </div>
                    </SwiperSlide>
                )}
                </Swiper>
                </div>
                
            </div>
        </React.Fragment>
    )
}

export default Testimonials;