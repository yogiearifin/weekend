import React from "react"
import "../assets/styles/pov.scss"

const Definition= () => {
    return(
        <React.Fragment>
            <div className="pov">
                <div className="pov-container">
                    <h1>POV</h1>
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ea 
                    commodo consequat. Duis aute irure dolor in reprehenderit in voluptate 
                    velit esse cillum dolore eu fugiat nulla pariatur. 
                    </p>
                </div>
                <div className="resource">
                    <h1>Resource</h1>
                    <p>
                    These cases are perfectly simple and easy to distinguish. 
                    In a free hour, when our power of choice is untrammelled and when nothing 
                    prevents our being able to do what we like best.
                    </p>
                    <img src={require("../assets/images/path-3.png")} alt="path-3" />
                </div>
            </div>
        </React.Fragment>
    )
}

export default Definition;