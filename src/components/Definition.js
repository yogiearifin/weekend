import React from "react"
import "../assets/styles/definition.scss"
import Testimonials from "./Testimonials"

const Definition= () => {
    return(
        <React.Fragment>
            <div className="definition">
                <img src={require("../assets/images/group-4.png")} alt="background" />
                <div className="definition-text">
                    <p>
                    <span>Definition;</span> a practice or exercise to test or improve one's fitness 
                    for athletic competition, ability, or performance to exhaust 
                    (something, such as a mine) by working to devise, arrange, or achieve by 
                    resolving difficulties. Merriam-Webster.com Dictionary.
                    </p>
                    <p className="definition-team">
                    -weekend team
                    </p>
                </div>
                <img src={require("../assets/images/oval.png")} className="blue-dot" alt="blue-dot" />
            </div>
            <div className="testimony">
                <h1>Testimonials</h1>
            </div>
            <Testimonials />
        </React.Fragment>
    )
}

export default Definition;