import React from "react"
import { Button } from 'reactstrap';
import "../assets/styles/weekend.scss";

const Weekend = () => {
    return(
        <React.Fragment>
            <div className="weekend">
                <div className="weekend-container">
                    <h1>Weekend From Home</h1>
                    <p>Stay active with a little workout.</p>
                    <img src={require("../assets/images/bitmap.png")} alt="bitmap" />
                    <Button>Let's Go</Button>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Weekend;