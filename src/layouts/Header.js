import React from "react"
import { Navbar,  NavbarBrand } from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../assets/styles/header.scss"

const Header = (props) => {;
  
    return (
      <React.Fragment>
      <div className="header">
        <Navbar color="faded" light>
          <NavbarBrand href="/" className="logo">
            <div className="header-logo">
              <img src={require("../assets/images/bitmap_2.png")} alt="logo" />
            </div>
            </NavbarBrand>
          <div>
            <p>Good Morning</p>
            <h5>Fellas</h5>
          </div>
        </Navbar>
      </div>
      </React.Fragment>
    );
  }
  
  export default Header;